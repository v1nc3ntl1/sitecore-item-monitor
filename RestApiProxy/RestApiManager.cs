﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;


namespace RestApiProxy
{
    public class RestApiManager
    {
        public static T Get<T>(string endpoint, Dictionary<string, string> headers)
        {
            using (var httpClient = new HttpClient())
            {
                if (headers != null)
                {
                    foreach (var key in headers.Keys)
                    {
                        httpClient.DefaultRequestHeaders.Add(key, headers[key]);
                    }
                }

                var response = httpClient.GetStringAsync(new Uri(endpoint)).Result;

                return response != null ? JsonConvert.DeserializeObject<T>(response) : default(T);
            }
        }

        public static bool Post<T>(string endpoint, Dictionary<string, string> headers, T body)
        {
            using (var httpClient = new HttpClient())
            {
                using (var request = new HttpRequestMessage(HttpMethod.Post, endpoint))
                {
                    if (headers != null)
                    {
                        foreach (var key in headers.Keys)
                        {
                            httpClient.DefaultRequestHeaders.Add(key, headers[key]);
                        }
                    }

                    var response = httpClient.PostAsync(new Uri(endpoint), new StringContent(JsonConvert.SerializeObject(body))).Result;

                    return response.IsSuccessStatusCode;
                }
            }
        }
    }
}
