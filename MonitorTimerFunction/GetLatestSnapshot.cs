using System.Linq;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Proxy;
using RestApiProxy;
using Microsoft.WindowsAzure.Storage;
using System.Collections.Generic;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using Newtonsoft.Json;
using System.IO;
using System.Text;

namespace MonitorTimerFunction
{
    public static class GetLatestSnapshot
    {
        private static string sitecoreSscEndpoint = ConfigurationManager.AppSettings["sitecoreEndpoint"];
        private static List<object> allItems;
        private static string storageConnectionString = ConfigurationManager.AppSettings["ConnectionString"];
        private static readonly string blobName = ConfigurationManager.AppSettings["BlobName"];
        private static readonly string storageContainer = ConfigurationManager.AppSettings["StorageContainer"];
        private static readonly string archivedStorageContainer = ConfigurationManager.AppSettings["ArchivedStorageContainer"];
        private static string monitoringItemId = ConfigurationManager.AppSettings["monitoringItemId"];

        [FunctionName("GetLatestSnapshot")]
        public static async Task<HttpResponseMessage> Run([HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)]HttpRequestMessage req, TraceWriter log)
        {
            log.Info("C# HTTP trigger function processed a request.");

            var appSettingsBuilder = new StringBuilder(200);
            appSettingsBuilder.AppendLine("Printing appsettings key value:");
            foreach (var key in ConfigurationManager.AppSettings.AllKeys)
            {
                appSettingsBuilder.AppendFormat("{0}:{1}", key, ConfigurationManager.AppSettings[key]);
            }
            log.Info(appSettingsBuilder.ToString());

            allItems = RestApiManager.Get<List<object>>($"{sitecoreSscEndpoint}/sitecore/api/ssc/item/{monitoringItemId}/children", null);

            var oldArchivedName = CreateLatestSnapshot(allItems, log);

            return req.CreateResponse(HttpStatusCode.OK, $"successfully upload create latest item {monitoringItemId} snapshot. Archived snapthot: " + oldArchivedName);
        }

        private static string CreateLatestSnapshot(List<object> list, TraceWriter log)
        {
            // Check whether the connection string can be parsed.
            CloudStorageAccount storageAccount;
            if (!CloudStorageAccount.TryParse(storageConnectionString, out storageAccount))
            {
                log.Info($"Invalid Connection string: {storageConnectionString}");
            }

            // Create the CloudBlobClient that represents the Blob storage endpoint for the storage account.
            CloudBlobClient cloudBlobClient = storageAccount.CreateCloudBlobClient();

            // Create a container called 'quickstartblobs' and append a GUID value to it to make the name unique. 
            CloudBlobContainer cloudBlobContainer = cloudBlobClient.GetContainerReference(storageContainer);

            CloudBlobContainer archivedCloudBlobContainer = cloudBlobClient.GetContainerReference(archivedStorageContainer);

            GetCloudBlobContainer(cloudBlobContainer);

            GetCloudBlobContainer(archivedCloudBlobContainer);

            return UploadFile(cloudBlobContainer, archivedCloudBlobContainer, log, list).Result;
        }

        private async static void GetCloudBlobContainer(CloudBlobContainer cloudBlobContainer)
        {
            if (cloudBlobContainer.Exists()) return;
            
            await cloudBlobContainer.CreateAsync();

            // Set the permissions so the blobs are public. 
            BlobContainerPermissions permissions = new BlobContainerPermissions
            {
                PublicAccess = BlobContainerPublicAccessType.Blob
            };

            await cloudBlobContainer.SetPermissionsAsync(permissions);
        }

        private async static Task<string> UploadFile(CloudBlobContainer cloudBlobContainer, CloudBlobContainer archivedCloudBlobContainer, TraceWriter log, List<object> items)
        {
            var archivedFileName = string.Empty;
            CloudBlockBlob cloudBlockBlob = cloudBlobContainer.GetBlockBlobReference(blobName);

            if (cloudBlockBlob.Exists())
            {
                // Archived old blob
                var oldBlobName = $"{blobName}_before{DateTime.Now.ToString("yyyyMMdd_hhmm")}";
                var cloudArchiveBlockBlob = archivedCloudBlobContainer.GetBlockBlobReference(oldBlobName);
                cloudArchiveBlockBlob.StartCopy(cloudBlockBlob);
                //cloudBlockBlob.BeginDelete((blob) => { log.Info($"Item blob data: {blob.ToString()}"); }, blobName);
                archivedFileName = oldBlobName;
            }

            var serializedItems = JsonConvert.SerializeObject(items);

            try
            {
                cloudBlockBlob = cloudBlobContainer.GetBlockBlobReference(blobName);
                var cloudBlobStream = await Task.Factory.FromAsync(
                    (cb, s) => cloudBlockBlob.BeginOpenWrite(
                        AccessCondition.GenerateEmptyCondition(),
                        new BlobRequestOptions(),
                        new OperationContext(),
                        cb, s),
                    ar => cloudBlockBlob.EndOpenWrite(ar),
                    null);

                using (var writer = new StreamWriter(cloudBlobStream))
                {
                    await writer.WriteAsync(serializedItems);
                }
            }
            catch (StorageException ex)
            {
                log.Info($"Error Updating snapshot: {ex.Message}");
                if (ex.RequestInformation != null && ex.RequestInformation.HttpStatusCode == 409)
                {
                    // Blob already existed!
                    throw new InvalidOperationException("CloudAuditingService_DuplicateAuditRecord");
                }
                throw;
            }

            return archivedFileName;
        }
    }
}
