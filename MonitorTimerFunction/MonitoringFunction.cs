using System;
using System.Collections.Generic;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Proxy;
using System.Linq;
using RestApiProxy;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.IO;
using System.Threading.Tasks;
using System.Threading;
using System.Configuration;
using Newtonsoft.Json;
using System.Text;

namespace MonitorTimerFunction
{
    public static class MonitoringFunction
    {
        #region field
        private static string sitecoreSscEndpoint = ConfigurationManager.AppSettings["sitecoreEndpoint"];
        private static IEnumerable<string> monitoringChildrenIds = string.IsNullOrEmpty(ConfigurationManager.AppSettings["monitoringIds"].ToString()) ? new List<string>() : ConfigurationManager.AppSettings["monitoringIds"].Split('|').Where(i => !string.IsNullOrEmpty(i));
        private static string slackUrlslackWebHookUrl = ConfigurationManager.AppSettings["slackWebHookUrl"];
        private static IEnumerable<string> mentions = ConfigurationManager.AppSettings["Mention"].Split('|');
        private static string successEmoji = ConfigurationManager.AppSettings["SuccessEmoji"];
        private static string failedEmoji = ConfigurationManager.AppSettings["FailedEmoji"];
        private static string monitoringItemId = ConfigurationManager.AppSettings["monitoringItemId"];
        private static string showCorrectItem = ConfigurationManager.AppSettings["isNotifyCorrectItem"];
        private static string storageConnectionString = ConfigurationManager.AppSettings["ConnectionString"];
        private static readonly string blobName = ConfigurationManager.AppSettings["BlobName"];
        private static readonly string storageContainer = ConfigurationManager.AppSettings["StorageContainer"];
        private static readonly string functionKey = ConfigurationManager.AppSettings["FunctionKey"];

        private static List<object> allItems;
        private static string BlobSnapshotUrl;
        #endregion

        #region Properties
        public static bool ShowCorrectItem
        {
            get
            {
                if (showCorrectItem == null) return false;

                bool result = false;
                return Boolean.TryParse(showCorrectItem, out result) ? result : false;
            }
        }

        #endregion

        [FunctionName("MonitoringFunction")]
        public static async void Run([TimerTrigger("0 */5 * * * *")]TimerInfo myTimer, TraceWriter log)
        {
            log.Info($"C# Timer trigger function executed at: {DateTime.Now}");
            BlobSnapshotUrl = null;
            LogAppSettings(log);

            allItems = RestApiManager.Get<List<object>>($"{sitecoreSscEndpoint}/sitecore/api/ssc/item/{monitoringItemId}/children", null);
            log.Info($"Items count: {allItems.Count}");

            var allItemsDictionary = new Dictionary<string, object>();
            foreach (var r in allItems)
            {
                allItemsDictionary.Add((r as dynamic).ItemID.ToString(), r);
            }
            log.Info($"Added Items to dictionary? Dictionary count: {allItemsDictionary.Count}");

            var itemsSnapshot = ReadItems(allItems, log);
            log.Info($"Read Item snapshot into memory. Items count: {itemsSnapshot.Count}");

            var helper = new NotifyHelper();

            log.Info($"Comparing items started.");

            foreach (var itemKey in allItemsDictionary.Keys)
            {
                if (!monitoringChildrenIds.Any() || monitoringChildrenIds.Contains(itemKey.ToLower()))
                {
                    CheckItem(new Tuple<string, object>(itemKey, allItemsDictionary[itemKey]), itemsSnapshot, helper, log);
                }
            }

            log.Info($"Comparing items complete.");

            if (allItemsDictionary.Count != itemsSnapshot.Count)
            {
                helper.Success = false;
                var missingMessage = $"Items {monitoringItemId} count mismatched with snapshot. Expected count: {itemsSnapshot.Keys.Count} \n Actual count: {allItemsDictionary.Count}";

                foreach (var key in itemsSnapshot.Keys)
                {
                    if (!allItemsDictionary.ContainsKey(key))
                    {
                        missingMessage = $"{missingMessage}\n,{key}";
                    }
                }

                helper.Notify($"Children of items {monitoringItemId} are missing. Following are the missing item: \n{missingMessage}");
                log.Info($"Children of items  {monitoringItemId} are missing. Following are the missing item: \n{missingMessage}");
            }

            helper.Notify($"\n View Snapshot: {GetSnapshotUrl(log)}\n");

            helper.Dispose();
        }

        private static bool CheckItem(Tuple<string, object> item, Dictionary<string, object> itemsDictionary, NotifyHelper helper, TraceWriter log)
        {
            if (!itemsDictionary.ContainsKey(item.Item1))
            {
                helper.Success = false;
                helper.Notify($"Items Snapshot doesn't have item with id {{{item.Item1}}}. Capture new snapshot for program to work correctly. \n");
                return false;
            }

            var itemSnapshot = itemsDictionary[item.Item1];

            if (!string.Equals(itemSnapshot.ToString(), item.Item2.ToString()))
            {
                helper.Success = false;
                //helper.Notify($"Items doesn't match with snapshot. REPUBLISH NOW !!!!!!!!!!!!!! \n Expected item = {JsonConvert.SerializeObject(itemSnapshot)} \n Actual item = {JsonConvert.SerializeObject(item)} \n");
                helper.Notify($"Item id {item.Item1} doesn't match with snapshot. REPUBLISH NOW !!!!!!!!!!!!!! \n\tSee the differences https://{Environment.GetEnvironmentVariable("WEBSITE_HOSTNAME")}/api/GetDifferences?code={functionKey}&id={item.Item1} \n\t\tSee the actual item: {sitecoreSscEndpoint}/sitecore/api/ssc/item/{item.Item1} \n\n");
                return false;
            }

            helper.Success = true;
            if (ShowCorrectItem) helper.Notify($"Correct Item id: {item.Item1} \n");

            return true;
        }

        private static string PopulateSuccessEmoji()
        {
            if (string.IsNullOrEmpty(successEmoji)) return ":raised_hands:";

            return successEmoji;
        }

        private static string PopulateFailedEmoji()
        {
            if (string.IsNullOrEmpty(failedEmoji)) return ":scream:";

            return failedEmoji;
        }

        private static CloudStorageAccount GetStorageAccount(TraceWriter log)
        {
            CloudStorageAccount storageAccount;
            if (!CloudStorageAccount.TryParse(storageConnectionString, out storageAccount))
            {
                log.Info($"Invalid Connection string: {storageConnectionString}");
            }

            return storageAccount;
        }

        private static string GetSnapshotUrl(TraceWriter log)
        {
            if (!string.IsNullOrEmpty(BlobSnapshotUrl)) return BlobSnapshotUrl;

            CloudStorageAccount storageAccount = GetStorageAccount(log);
            // Create the CloudBlobClient that represents the Blob storage endpoint for the storage account.
            CloudBlobClient cloudBlobClient = storageAccount.CreateCloudBlobClient();

            // Create a container called 'quickstartblobs' and append a GUID value to it to make the name unique. 
            CloudBlobContainer cloudBlobContainer = cloudBlobClient.GetContainerReference(storageContainer);

            CloudBlockBlob cloudBlockBlob = cloudBlobContainer.GetBlockBlobReference(blobName);

            BlobSnapshotUrl = cloudBlockBlob.Uri.ToString();

            return BlobSnapshotUrl;
        }

        private static Dictionary<string, object> ReadItems(List<object> allItems, TraceWriter log)
        {
            // Check whether the connection string can be parsed.
            CloudStorageAccount storageAccount = GetStorageAccount(log);

            // Create the CloudBlobClient that represents the Blob storage endpoint for the storage account.
            CloudBlobClient cloudBlobClient = storageAccount.CreateCloudBlobClient();

            // Create a container called 'quickstartblobs' and append a GUID value to it to make the name unique. 
            CloudBlobContainer cloudBlobContainer = cloudBlobClient.GetContainerReference(storageContainer);

            GetCloudBlobContainer(cloudBlobContainer);

            UploadFile(cloudBlobContainer, log, allItems);

            var output = ReadFile(cloudBlobContainer, log);

            var cast = JsonConvert.DeserializeObject<List<object>>(output);

            if (cast == null)
            {
                log.Info($"Invalid file format: {blobName}");
                return null;
            }

            var dictionary = new Dictionary<string, object>();
            foreach (var o in cast)
            {
                dictionary.Add((o as dynamic).ItemID.ToString(), o);
            }

            return dictionary;
        }

        private static void LogAppSettings(TraceWriter log)
        {
            log.Info("Showing all app settings: ");
            foreach (var key in ConfigurationManager.AppSettings.AllKeys)
            {
                log.Info($"\t{key}: {ConfigurationManager.AppSettings[key]}");
            }
        }

        private async static void GetCloudBlobContainer(CloudBlobContainer cloudBlobContainer)
        {
            if (cloudBlobContainer.Exists()) return;

            await cloudBlobContainer.CreateAsync();

            // Set the permissions so the blobs are public. 
            BlobContainerPermissions permissions = new BlobContainerPermissions
            {
                PublicAccess = BlobContainerPublicAccessType.Blob
            };

            await cloudBlobContainer.SetPermissionsAsync(permissions);
        }

        private async static void UploadFile(CloudBlobContainer cloudBlobContainer, TraceWriter log, List<object> items)
        {
            CloudBlockBlob cloudBlockBlob = cloudBlobContainer.GetBlockBlobReference(blobName);

            if (cloudBlockBlob.Exists())
            {
                return;
            }

            var serializedItems = JsonConvert.SerializeObject(items);

            try
            {
                cloudBlockBlob = cloudBlobContainer.GetBlockBlobReference(blobName);
                var cloudBlobStream = await Task.Factory.FromAsync(
                    (cb, s) => cloudBlockBlob.BeginOpenWrite(
                        AccessCondition.GenerateIfNoneMatchCondition("*"),
                        new BlobRequestOptions(),
                        new OperationContext(),
                        cb, s),
                    ar => cloudBlockBlob.EndOpenWrite(ar),
                    null);

                using (var writer = new StreamWriter(cloudBlobStream))
                {
                    await writer.WriteAsync(serializedItems);
                }
            }
            catch (StorageException ex)
            {
                if (ex.RequestInformation != null && ex.RequestInformation.HttpStatusCode == 409)
                {
                    // Blob already existed!
                    throw new InvalidOperationException("CloudAuditingService_DuplicateAuditRecord");
                }
                throw;
            }
        }

        private static string ReadFile(CloudBlobContainer cloudBlobContainer, TraceWriter log)
        {
            string text;
            CloudBlockBlob cloudBlockBlob = cloudBlobContainer.GetBlockBlobReference(blobName);

            using (var waitHandle = new AutoResetEvent(false))
            {
                var result = cloudBlockBlob.BeginOpenRead(new AccessCondition(), null /* options */, null /* operationContext */, ar => waitHandle.Set(), null);
                waitHandle.WaitOne();
                Stream stream = cloudBlockBlob.EndOpenRead(result);
                using (var reader = new StreamReader(stream))
                {
                    text = reader.ReadToEnd();
                }
            }

            return text;
        }

        public class NotifyHelper : IDisposable
        {
            private StringBuilder sb = new StringBuilder();
            private bool _success = true;

            public bool Success
            {
                get
                {
                    return _success;
                }
                set
                {
                    if (_success)
                    {
                        _success = value;
                    }
                }
            }

            public void Dispose()
            {
                //temp slack notify
                var formattedMention = string.Empty;

                if (!Success)
                {
                    foreach (var mention in mentions)
                    {
                        if (string.IsNullOrEmpty(mention)) continue;
                        formattedMention = $"{formattedMention} <@{mention}>";
                    }
                }

                // populate emoji
                var emoji = Success ? PopulateSuccessEmoji() : PopulateFailedEmoji();

                if (Success)
                {
                    sb.Append("Nothing happened. Chilled Mate.\n");
                }

                RestApiManager.Post<SlackRequestBody>($"{slackUrlslackWebHookUrl}", null,
                    new SlackRequestBody() { Text = $"{formattedMention} {emoji} {sb.ToString()}" });
            }

            public void Notify(string message)
            {
                sb.Append(message);
            }
        }

        public class SlackRequestBody
        {
            [JsonProperty("text")]
            public string Text { get; set; }
        }
    }
}
