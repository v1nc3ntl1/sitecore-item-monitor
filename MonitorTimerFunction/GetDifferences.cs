using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using JsonDiffPatchDotNet;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestApiProxy;

namespace MonitorTimerFunction
{
    public static class GetDifferences
    {
        private static string sitecoreSscEndpoint = ConfigurationManager.AppSettings["sitecoreEndpoint"];
        private static readonly string storageContainer = ConfigurationManager.AppSettings["StorageContainer"];
        private static string storageConnectionString = ConfigurationManager.AppSettings["ConnectionString"];
        private static readonly string blobName = ConfigurationManager.AppSettings["BlobName"];

        [FunctionName("GetDifferences")]
        public static async Task<HttpResponseMessage> Run([HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)]HttpRequestMessage req, TraceWriter log)
        {
            log.Info("C# HTTP trigger function processed a request.");

            // parse query parameter
            string id = req.GetQueryNameValuePairs()
                .FirstOrDefault(q => string.Compare(q.Key, "id", true) == 0)
                .Value;

            if (id == null)
            {
                // Get request body
                dynamic data = await req.Content.ReadAsAsync<object>();
                id = data?.id;
            }

            if (id == null)
            {
                return req.CreateResponse(HttpStatusCode.BadRequest, "Please pass a id on the query string or in the request body");
            }

            if (!Guid.TryParse(id, out var itemId))
            {
                return req.CreateResponse(HttpStatusCode.BadRequest, "Please pass a id on the query string or in the request body");
            }

            object sitecoreItem = RestApiManager.Get<object>($"{sitecoreSscEndpoint}/sitecore/api/ssc/item/{itemId}", null);

            if (sitecoreItem == null)
            {
                return req.CreateResponse(HttpStatusCode.NotFound, "Not found when call sitecore ssc");
            }

            JToken diff = null;
            try
            {
                Dictionary<string, object>  snapshotItems = GetLatestSnapshot();

                if (!snapshotItems.ContainsKey(id))
                {
                    return req.CreateResponse(HttpStatusCode.NotFound);
                }

                var matchItem = snapshotItems[id];

                var jsonDiffer = new JsonDiffPatch();
                var actual = JToken.Parse(sitecoreItem.ToString());
                var expected = JToken.Parse(matchItem.ToString());
                diff = jsonDiffer.Diff(expected, actual);
            }
            catch (Exception ex)
            {
                req.CreateResponse(HttpStatusCode.InternalServerError, "Error", ex.ToString());
            }

            return new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(diff.ToString(), System.Text.Encoding.UTF8, "application/json")
            };
            //return req.CreateResponse(HttpStatusCode.OK, diff, System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
        }

        private static CloudBlobContainer GetStorageContainer()
        {
            CloudStorageAccount storageAccount;
            if (!CloudStorageAccount.TryParse(storageConnectionString, out storageAccount))
            {
                throw new Exception($"Invalid Connection string: {storageConnectionString}");
            }

            // Create the CloudBlobClient that represents the Blob storage endpoint for the storage account.
            CloudBlobClient cloudBlobClient = storageAccount.CreateCloudBlobClient();

            // Create a container called 'quickstartblobs' and append a GUID value to it to make the name unique. 
           return cloudBlobClient.GetContainerReference(storageContainer);
        }

        private static Dictionary<string, object> GetLatestSnapshot()
        {
            CloudBlobContainer cloudBlobContainer = GetStorageContainer();

            CloudBlockBlob cloudBlockBlob = cloudBlobContainer.GetBlockBlobReference(blobName);

            var output = ReadFile(cloudBlobContainer);

            var cast = JsonConvert.DeserializeObject<List<object>>(output);

            if (cast == null)
            {
                throw new Exception($"Invalid file format: {blobName}");
            }

            var dictionary = new Dictionary<string, object>();
            foreach (var o in cast)
            {
                dictionary.Add((o as dynamic).ItemID.ToString(), o);
            }

            return dictionary;
        }

        private static string ReadFile(CloudBlobContainer cloudBlobContainer)
        {
            string text;
            CloudBlockBlob cloudBlockBlob = cloudBlobContainer.GetBlockBlobReference(blobName);

            using (var waitHandle = new AutoResetEvent(false))
            {
                var result = cloudBlockBlob.BeginOpenRead(new AccessCondition(), null /* options */, null /* operationContext */, ar => waitHandle.Set(), null);
                waitHandle.WaitOne();
                Stream stream = cloudBlockBlob.EndOpenRead(result);
                using (var reader = new StreamReader(stream))
                {
                    text = reader.ReadToEnd();
                }
            }

            return text;
        }
    }
}
